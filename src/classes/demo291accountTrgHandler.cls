public class demo291accountTrgHandler {
    
    public static void validateAccount(List<account> acclist ) {
        
        for(account acc : acclist) {
            if(acc.CustomerPriority__c == 'High') {
                acc.CustomerPriority__c.addError('@@@ You can not enter the record priority as High');
            } else {                
                acc.CustomerPriority__c = 'Low';
            }
        }
    }
    
    public static void createContact(List<account> acclist) {
        
        List<contact> conlist = new List<contact>();
        Contact con;
        for(account acc : acclist) {
            con = new contact();
            con.LastName = acc.Name;
            con.AccountId = acc.Id;
            con.Phone =  acc.Phone;
            conlist.add(con);            
        }
        insert conlist;
    }
}