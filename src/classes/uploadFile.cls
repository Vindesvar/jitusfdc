public class uploadFile {
    //public String Type { get; set; }
    public String fN { get; set; }
    public blob file { get; set; }
    public String title{get;set;}
    public ContentVersion cv { get; set; }
    //public String ConType{get;set;}

public PageReference upload() {
    ContentVersion cv = new ContentVersion();    
    cv.title = title;
    cv.versionData=file;
    cv.pathOnClient = title;

        try
        {
            insert cv;
            system.debug('*********************Result'+cv.pathonClient);
        }
        catch (DMLException e)
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading Document in Library'));
            return null;
        }

            ContentVersion insertContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :CV.Id];                       
            ContentWorkspace insertWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name ='MyLibrary'];
            ContentWorkspaceDoc insertWorkSpaceDoc = new ContentWorkspaceDoc();        
            insertWorkSpaceDoc.ContentWorkspaceId = insertWorkspace.Id;                
            insertWorkSpaceDoc.ContentDocumentId = insertContent.ContentDocumentId;        
            insert insertWorkSpaceDoc ;
        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Document uploaded successfully to library'));
        pagereference  p = new pagereference ('/'+cv.Id);
        return p;
    }
}