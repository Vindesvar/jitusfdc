global class batch_class1 implements Database.Batchable <SObject> {
    
    //global final String query;
    //global final String s_Object;
   // global final String field;
    //global final String field_value;
    
   /* global batch_class1(String q, String s, String f, String v) {
    
        query = q;
        s_Object = s;
        field = f;
        field_value = v;
        
        system.debug('batch_class1>>>>>>>>>>>>');
    }*/
    
    global Database.QueryLocator start(Database.BatchableContext BC ) {
      system.debug('start>>>>>>>>>>>>>>>>>>');
        String query = 'select industry from Account limit 4';
        return Database.getQueryLocator(query);
                  
    }
    
    
    global void execute (Database.BatchableContext BC, List<Account> scope) {
            
        for( Account so : scope) {
            //so.put('Industry', 'Cunsulting for check to finish');
            so.industry = 'Cunsulting for check to finish Update';
        }
                        system.debug('execute>>>>>>>>>>>>>');
        update scope;
    
    }
    
    global void finish (Database.BatchableContext BC) {
          system.debug('finishMethod>>>>>>>>>>>>>');
    }
    
}














/*

public class HIPIDemoforRetention {
    
    
    public void HIPIDemoRetentionBatchDelete(Set<Id> SetContractId){
                      
        List<ContentVersion> conDocLst = [select Id, ContentDocumentId, Contract__r.Contract_SBG__c from ContentVersion where Contract__r.Retention__c != 'Delete' AND Contract__r.id  in : SetContractId];              
        system.debug('conDocId>>>>>>>>>>>>>>>>>>>' + conDocLst);
        
        List<ContentDocument> CdList = new List<ContentDocument>();                             
        
        
        
        Set<ID> CDId = new Set<ID>();
        
        for(ContentVersion cd : conDocLst) {
            
            CDId.add(cd.ContentDocumentId);
        }
        
        CdList = [Select Id, ParentId from ContentDocument where Id IN : CDId];
        
        ContentWorkSpace libId = new ContentWorkSpace();        
        //ContentWorkSpace delLibId = [Select Id, Name from ContentWorkSpace where Name = 'HIPI Deletion Library'];  
        system.debug('delLibId>>>>>>>>>>>>>>>>>>>' + CdList);
        
        List<ContentDocument> CD_Update = new List<ContentDocument>();               
        
        for(ContentVersion cv : conDocLst) {
            
            String libName = '%' + cv.Contract__r.Contract_SBG__c + '%';
            system.debug('libName>>>>' + libName);
            libId = [Select Id from ContentWorkSpace where Name Like : libName];
            system.debug('libName>>>>' + libId.Id);
            for(ContentDocument CD : CdList)
            {
               CD.ParentId = libId.id;
               CD_Update.add(CD);
            }
          
            
        }
        //update CD_Update;
        system.debug('WS_Update>>>>>>>>>>>>>>>>>>>' + CD_Update);                

        
    }
    
    
    
    
    
    
   /***************************************************************************************************************/ 

    /*public void HIPIDemoRetentionBatchDelete() {
    
        List<contentversion> contentDocId = new List<contentversion>();
        List<contentdocument> CD_ToDelete = new List<contentdocument>();
        contentDocId = [select Id,contract__r.Id, contentDocumentId, contract__r.Retention__c from contentversion where contract__r.Retention__c ='Archive'];
        
        system.debug('contentDocId>>>>>>>>>>' + contentDocId);
        
        Set<ID> conDocId = new Set<ID>();
        
        for (contentversion cv : contentDocId) {
            
            conDocId.add(cv.contentDocumentId);
        }
        system.debug('conDocId>>>>>>>>>>' + conDocId);
        
        if(conDocId !=null){
            
        CD_ToDelete = [SELECT Id FROM contentdocument where Id IN : conDocId];
        Delete CD_ToDelete;
        system.debug('CD_ToDelete>>>>>>>>>>' + CD_ToDelete); 
        }
        
    } */
   
    
    
   /***************************************************************************************/ 
    
   /* public void HIPIDemoforRetentionBatch() {         
  
       List<Contract> retLst = new List<Contract>();
            retLst = [Select Id, Retention__c,Contract_End_Date__c,StartDate,Status__c,Old_Contract_Code__c,New_Contract_Code__c from contract where (Status__c = 'Expired ' OR Status__c = 'Terminated ' OR Status__c = 'Bankruptcy' OR Status__c <> 'Executed') AND (Retention__c <> 'Retain' AND Retention__c <> 'Litigation Hold' AND Retention__c <> 'Delete' AND Retention__c <> 'Retain - Other') AND(Old_Contract_Code__c <> null) AND (New_Contract_Code__c <> null)];
            system.debug('retLst>>>>>>>>>>>>>>>>>>'+retLst);  
                
            List<Contract> lstContractToUpdate = new List<Contract>();
            
            Set<ID> contRetainId = new Set<ID>(); 
            
            for (Contract ct : retLst) {
            if(ct.Contract_End_Date__c > ct.StartDate.addyears(7).adddays(1))
            {
             ct.Retention__c = 'Archive';
             lstContractToUpdate.add(ct);
             contRetainId.add(ct.Id);
            }
            }
            system.debug('contRetainId>>>>>>>>>>>>>>>>>>'+contRetainId);
            system.debug('lstContractToUpdate>>>>>>>>>>>>>>>>>>'+lstContractToUpdate);
            //update lstContractToUpdate ;
                
                
            List<ContentVersion> CV = [Select contract__r.id, ContentDocumentId from ContentVersion where contract__r.id IN : contRetainId ];
            system.debug('CV>>>>>>>>>>>>>>>>>>'+CV);                
            Set<ID> contDocId = new Set<ID>();
            
            for (ContentVersion d : CV) {
            
                contDocId.add(d.ContentDocumentId);
            }
            system.debug('contDocId>>>>>>>>>>>>>>>>>>'+contDocId);                
        
            List<ContentDocument> CD = [Select Id, ParentId from ContentDocument where Id IN : contDocId];
            system.debug('CD>>>>>>>>>>>>>>>>>>'+CD);                
            Set<ID> CDId = new Set<ID>();
            
            ID  TargetWS = [select Id, Name from contentworkspace where name = 'HIPI Deletion Library'].Id;
            system.debug('TargetWS>>>>>>>>>>>>>>>>>>'+TargetWS);                
            List<ContentDocument> conDocToUpd = new  List<ContentDocument>();
            
            for (ContentDocument c : CD) {
            
                    c.ParentId = TargetWS;
                    conDocToUpd.add(c);
            }
            //update conDocToUpd;
            
          system.debug('conDocToUpd>>>>>>>>>>>>>>>>>>'+conDocToUpd); 
         
       } */
   
        
        
        
//}