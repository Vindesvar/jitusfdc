public  class Xmlparsar
{
    //xml string
    public String xmlstring{get;set;}
    
    public String OrderDetailId{get;set;}
    public String prdCode {get;set;}
  
    //display xml string
    public String outxmlstring{get;set;}
  
    //rootelement
    public String rootElement{get;set;}
  
    //
    public String filename{get;set;}
  
    public blob body{get;set;}
     
    //constructor
    public Xmlparsar()
    {
    
       string xml = '<OrderDetails> <OrderDetailID>2584</OrderDetailID><GiftWrapCost>0.0000</GiftWrapCost><GiftWrapNote/><ProductCode>800006</ProductCode></OrderDetails>';
       Dom.Document doc = new Dom.Document();
       doc.load(xml);

        //Retrieve the root element for this document.
        Dom.XMLNode ordDtls = doc.getRootElement();
        // Alternatively, loop through the child elements.
        // This prints out all the elements of the address
        for(Dom.XMLNode child : ordDtls.getChildElements()) {
              if(child.getText()!= null)
              {   
              if(child.getName() == 'ProductCode') {
                 OrderDetailId = (child.getText()) + '  '+child.getName();
               }      
          }
        }        
    }
  
   
//Parsing xml what you entered in the left text area
    public pagereference Parsexml()
    {
       DOM.Document xmlDOC = new DOM.Document();
       xmlDOC.load(xmlstring);
       DOM.XMLNode rootElement = xmlDOC.getRootElement();
       //outxmlstring=String.valueof(xmlDOC.getRootElement().getName());
        //Dom.XMLNode ordDtls = doc.getRootElement();

        //OrderDetailId = rootElement.getChildElement('SAP_Customer_Code', null).getText();
       // prdCode = rootElement.getChildElement('SAP_Customer_Code', null).getText();       
       
       
       system.debug('outxmlstring>>>>>>>>' + outxmlstring);
       system.debug('xmlDOC.getRootElement().getChildElements()>>>>>>>>>'+ xmlDOC.getRootElement().getChildElements());
       for(DOM.XMLNode xmlnodeobj:xmlDOC.getRootElement().getChildElements())
       //.getChildren())
       {       
      
          loadChilds(xmlnodeobj);        
       }
    return null;
  }
    //loading the child elements
    
    private void loadChilds(DOM.XMLNode xmlnode)
    {
        for(Dom.XMLNode child : xmlnode.getChildElements())
        {
          if(child.getText()!= null)
          {   
              if(child.getName() == 'SAP_Customer_Code') {
                  outxmlstring+=child.getText();
                }
      
          }
          loadChilds(child);      
        }
    }
  
  
//This is for parsing xml file what you selected
  public pagereference Parsexmlfile()
  {
       DOM.Document xmlDOC = new DOM.Document();
       xmlstring=body.tostring();        
       xmlDOC.load(xmlstring);
       DOM.XMLNode rootElement = xmlDOC.getRootElement();
      // outxmlstring=String.valueof(xmlDOC.getRootElement().getName());//gives you root element Name
       for(DOM.XMLNode xmlnodeobj:xmlDOC.getRootElement().getChildElements())
       {       
                 
          loadChilds(xmlnodeobj);
       }     
      return null;
    }
}