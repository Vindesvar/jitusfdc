public class AccountInseerAndShow {
    public Employee__c emp{get; set;}
    public Boolean flag {get; set;}
    
    public AccountInseerAndShow (){
        emp =  new Employee__c ();
        flag = true;
    }
    
    public pageReference save(){
        insert emp;
        flag = true;
        cancel();
        return null;
    }
    public pageReference cancel(){
        emp.Employee_ID__c = '';
        emp.Last_Name__c = '';
        emp.First_Name__c = ''; 
        emp.Phone_Number__c = '';
        flag = false ;
        return null;
    } 
    public List<JituLightening__Employee__c> getInsertedRecord(){
        List<JituLightening__Employee__c> listOfAccountObj = new List<JituLightening__Employee__c>();
        listOfAccountObj = [select id, JituLightening__Employee_ID__c,JituLightening__First_Name__c,JituLightening__Last_Name__c,JituLightening__Phone_Number__c,createdDate from JituLightening__Employee__c where createdDate = TODAY];
        return listOfAccountObj ;
    }   
}