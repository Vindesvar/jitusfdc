public class AddErrorMessage {
    
    Public Account ObjectOfAccount{get; set;}
    public AddErrorMessage(){
        ObjectOfAccount = new Account();
    }
    public pageReference saveaccountdata(){
        Account insertAccountRecord = new Account();
        insertAccountRecord.Name = ObjectOfAccount.Name;
        insertAccountRecord.Type = ObjectOfAccount.Type;
        insertAccountRecord.Phone = ObjectOfAccount.Phone;
        insertAccountRecord.INdustry = ObjectOfAccount.INdustry;
        insertAccountRecord.Fax = ObjectOfAccount.Fax;
        try{
            insert insertAccountRecord;            
        }catch(DMLException exp){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please fill the required field'));   
        }
        //PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());//apex/AddErrorMessage
        PageReference pageRef = new PageReference('/'+insertAccountRecord.id);
        //pageRef.setRedirect(false);
        resetAccount();
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'One record is inserted'));   
        return null;//pageRef;        
    }
    
    public void resetAccount(){
        Account insertAccountRecord = new Account();
        ObjectOfAccount.Name = '';
        ObjectOfAccount.Type = '';
        ObjectOfAccount.Phone = '';
        ObjectOfAccount.INdustry = '';
        ObjectOfAccount.Fax = '';
    }

}