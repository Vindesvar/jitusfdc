public class batch_class implements Database.Batchable <SObject> {
    String value;
    String field;
    String query;
    
    public batch_class( String v, String f, String q) {
        
        value = v;
        field = f;
        query = q;
    }
    
    public Database.QueryLocator start(Database.BatchableContext con) {
        //return Database.QueryLocator('select Name, Description from Account');
        return Database.getQueryLocator(query);
    }
    
    public void execute ( Database.BatchableContext con, List<SObject> scope ) {
    
        //for ( Account a : scope ) {
           // a.put (Description, 'Upadated using Batch');
           
           for ( SObject a : scope ) {
                
                a.put (field, value);
        }
        
        update scope;
    }
    
    public void finish ( Database.BatchableContext con) {
        
    }
    
    
}