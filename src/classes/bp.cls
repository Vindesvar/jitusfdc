public class bp {

/*******************************************************************************
Name        : HIPIAddCtr
Created By  : Vindesvar Kumar 
Date        : 1st Feb
Reason      : Controller class for Adding new contract and editing contract Page
*******************************************************************************

public class HIPIAddCtr{  
   
   public ContentVersion CV_Old{get;set;}
   public Transient ContentVersion CV_New{get;set;}
   Public ContentWorkspace CW{get;set;}
   Public Transient blob InPutfile{get;set;}
   Public Id ConverID{get;set;}
   public boolean IsEdit{get;set;}
   public boolean IsAdd{get;set;}   
   public String contractNumber {get;set;}      
   public Transient blob versionData{get;set;}
   public String fileName{get;set;}
   public String Title{get;set;}   
   public String selectedConWSName{get;set;}
   public String selectedContFiles{get;set;}
   public String pageTitle{get;set;}
      
            
       public HIPIAddCtr (){
               ConverID = ApexPages.CurrentPage().getParameters().get('CVId');
               CV_Old = new ContentVersion();
               IsEdit = false;
               pageTitle = 'Add Content';   
               IsAdd = false;
               ////If ContentVersionId is not null 
               if(ConverID != null){
                   IsEdit = true;
                   pageTitle = 'Edit Content'; 
                   CV_Old = [select id, ContentUrl,PathonClient,ContentDocument.ParentId, Title,FirstPublishLocationId,PublishStatus, Origin,Category_Type__c,Category_Sub_Type__c,Contract__c  from contentversion where id =: ConverID limit 1];
                   if(CV_Old != null && (CV_Old.FirstPublishLocationId != null || CV_Old.FirstPublishLocationId != '')){
                       CW = [select  Id,name from ContentWorkspace where id =: CV_Old.ContentDocument.ParentId]; //ContentWorkSpace Name and ID are selected
                   }

               }                 
                              
            }
            
       //Get all libraries name in picklist     
       public List<SelectOption> getConWSNames() {                           
 
              List<SelectOption> conOptions= new List<SelectOption>();
              
              conOptions.add( new SelectOption('','--Select--'));
              for( ContentWorkspace con : [select Id,name from ContentWorkspace where Name LIKE '%LMS%'] ) {
                      conOptions.add( new SelectOption(con.Name,con.name));
              }
             return conOptions;
       }
       
   
    //Save method to add and update 
    public PageReference SaveContentversion() {
        // Comman code        
        // Method for inserting new contentVersion        
        // Method for editing Contetntversion
                                                  
        
                //If ContentVersionId is not null and ContentVersion has data        
                if(IsEdit && CV_Old != null){
                    //If Library name is selected  and add new record     
                    if(InPutfile != null){ 
                    
                    CV_New =  new ContentVersion();
                    CV_New.Title = fileName;
                    CV_New.PathOnClient = fileName;
                    CV_New.Contract__c = CV_Old.Contract__c;
                    CV_New.Category_Type__c = CV_Old.Category_Type__c;
                    CV_New.Category_Sub_Type__c = CV_Old.Category_Sub_Type__c;
                    //CV_New.Licensee__c = CV_Old.Licensee__c;

                                
                    CV_New.VersionData= InPutfile;
                    CV_New.ContentUrl = CV_Old.ContentUrl;  
                    
                   
                    insert  CV_New;
                    ContentVersion insertContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :CV_New.Id];
                               
                    ContentWorkspace insertWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name = : CW.Name];
                    ContentWorkspaceDoc insertWorkSpaceDoc = new ContentWorkspaceDoc();        
                    insertWorkSpaceDoc.ContentWorkspaceId = insertWorkspace.Id;
                        
                    insertWorkSpaceDoc.ContentDocumentId = insertContent.ContentDocumentId;        
                    insert insertWorkSpaceDoc ;
                    
                   //CV_Old.ContentDocument.ParentId = '058f00000005O8W'; // enter Deletion Libary id here-- corrently hardcoaded             
                   
                   ContentVersion contentDocId = [SELECT ContentDocumentId FROM ContentVersion where Id = : CV_Old.Id];
                   contentdocument CD_ToDelete = [SELECT Id FROM contentdocument where Id = : contentDocId.ContentDocumentId ];
                   Delete CD_ToDelete;            
                   
                   
                } 
               else {
            
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please select a file'));
                        return null;
                   }      
                 Pagereference pg = new Pagereference('/apex/HIPIVirtualFileRoom');
                 return pg;  
            
           
        } else {
                     //If Library name is selected  and add new record     
                if(selectedConWSName!= null && InPutfile != null){                  
              
                    CV_Old.Title = fileName;
                    CV_Old.versionData=InPutfile;
                    CV_Old.pathonclient = fileName;                       
                               
                    insert CV_Old; 
                    ContentVersion insertNewContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :CV_Old.Id];
                    //system.debug('contentURL-->>>>>>>'+testContent);
                    
                    ContentWorkspace insertNewWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name =:selectedConWSName];
                    ContentWorkspaceDoc insertNewWorkspaceDoc = new ContentWorkspaceDoc();        
                    insertNewWorkspaceDoc.ContentWorkspaceId = insertNewWorkspace.Id;
                    insertNewWorkspaceDoc.ContentDocumentId = insertNewContent.ContentDocumentId;        
                    insert insertNewWorkspaceDoc; 
                      
                    Pagereference pageRefer = new Pagereference ('/apex/HIPIVirtualFileRoom');
                    return pageRefer ; 
                       
                } else {
            
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please select a file and library'));
                        return null;
                    }            
            
            }        
        
        Pagereference pageRefe = new Pagereference ('/apex/HIPIVirtualFileRoom');
        return pageRefe ;
    }
    

          
     public Pagereference cancelContent() {
         Pagereference pageRef = new Pagereference ('/apex/HIPIVirtualFileRoom');       
            return pageRef;
     }  
 
       
}
*********************************Vindesvar Kumar : End Of The Code ************************************/            

}