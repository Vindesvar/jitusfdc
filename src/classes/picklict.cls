public class picklict {

    public ContentVersion file{get;set;}    
    public Blob blobFile{get;set;}
    public String conType{get;set;}
    public String fileName{get;set;}
    public String myLib{get;set;}    
    public String selectedConWSId{get;set;} 
    
    public picklict()
    {
     file = new ContentVersion();
    }
            

       public List<SelectOption> getConWSNames() {                      
              List<SelectOption> conOptions= new List<SelectOption>();
              conOptions.add( new SelectOption('','--Select--'));
              for( ContentWorkspace con : [select Id,name from ContentWorkspace ] ) {
                      conOptions.add( new SelectOption(con.name,con.name)); /*SelectOption list takes two parameters one is value and other one is label .In this case ContentWorkspace name as a label and Id is the value .*/
              }
             return conOptions; 
       }  
    
        
        public PageReference insertData() {
        
        if(selectedConWSId != null){
        file.Title = fileName;
        insert file;
        
        ContentVersion testContent = [SELECT ContentDocumentId FROM ContentVersion where Id = :file.Id];               
        ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name =:selectedConWSId];        
        ContentWorkspaceDoc newWorkspaceDoc = new ContentWorkspaceDoc();        
        newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id;
        newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId;        
        insert newWorkspaceDoc;        
        PageReference pg = new PageReference('/'+file.id);//testContentInsert.id);        
        return pg;//return testContentInsert.Id; */
        } else {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please select library'));
        return null;
        }
  }
  
  public pagereference cancelupload()
  {
          pagereference pr = new pagereference('/apex/contverUpload');                 
          return pr ;  
  }
   
}

/*
/*******************************************************************************
Name        : HIPIAddCtr
Created By  : Vindesvar Kumar 
Date        : 1st Feb
Reason      : Controller class for AddFile Page
*******************************************************************************/
/*public class HIPIAddCtr{
  
   public ContentVersion addMthd{get;set;}   
   public blob versionData{get;set;}
   public String fileName{get;set;}
   public String Title{get;set;}   
   public String selectedConWSName{get;set;}
            
       public List<SelectOption> getConWSNames() {
       
              List<SelectOption> conOptions= new List<SelectOption>();
              
              conOptions.add( new SelectOption('','--Select--'));
              for( ContentWorkspace con : [select Id,name from ContentWorkspace where Name LIKE '%HIPI%'] ) {
                      conOptions.add( new SelectOption(con.Name,con.name)); /*SelectOption list takes two parameters one is value and other one is label .In this case ContentWorkspace name as a label and Id is the value .*/
/*             }
             return conOptions;
       }   
 
    
   
   public HIPIAddCtr (){
       addMthd = new ContentVersion();  
       List<ContentVersion> ContentLst = new List<ContentVersion>();
       ContentLst = [SELECT Category_Type__c,Category_Sub_Type__c,PartyName__c,Contract__c FROM ContentVersion];                 
    }
    
    public Pagereference AddContent() {
    
        if(selectedConWSName!= null){                        
            insert addMthd; 
            ContentVersion testContent = [SELECT ContentDocumentId,FileType,ContentURL FROM ContentVersion where Id = :addMthd.Id];
            //system.debug('contentURL-->>>>>>>'+testContent);
            
            ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name =:selectedConWSName];
            ContentWorkspaceDoc newWorkspaceDoc = new ContentWorkspaceDoc();        
            newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id;
            newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId;        
            insert newWorkspaceDoc;   
            Pagereference pg = new Pagereference('/'+addMthd.id);
            return pg;        
        }else{
        
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please select library'));
            return null;
        }
        
    }

     public Pagereference updateContent() {

        if(selectedConWSName!= null){
            //addMthd.versionData = versionData;
            //addMthd.PathOnClient =  '/'+fileName;            
            insert addMthd; 
            ContentVersion testContent = [SELECT ContentDocumentId,FileType,ContentURL FROM ContentVersion where Id = :addMthd.Id];
            system.debug('contentURL---'+testContent);
            
            ContentWorkspace testWorkspace = [SELECT Id FROM ContentWorkspace WHERE Name =:selectedConWSName];
            ContentWorkspaceDoc newWorkspaceDoc = new ContentWorkspaceDoc();        
            newWorkspaceDoc.ContentWorkspaceId = testWorkspace.Id;
            newWorkspaceDoc.ContentDocumentId = testContent.ContentDocumentId;
            
            Set<String> conVerLst = new Set<String>();
            Set<String> docId = new Set<String>(); 
            
            for(ContentVersion conTitle :[select title from ContentVersion] ) {        
                conVerLst.add(conTitle.title);                  
            } 
            
            for(ContentVersion conDocId :[select contentDocumentId from ContentVersion] ) {        
                docId.add(conDocId.contentDocumentId);                  
            }                                   
            //for(ContentVersion c:getContVer) {
                if(conVerLst.contains(Title)) {
                 update newWorkspaceDoc;
                 Pagereference pg = new Pagereference('/'+addMthd.id);
                 return pg;                 
                }else {
                 insert newWorkspaceDoc; 
                 Pagereference pg = new Pagereference('/'+addMthd.id);
                 return pg;                                    
                } 
            //}
           return null; 
         }else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please select library'));
            return null;
          }                                           
       }  

          
     public Pagereference cancelContent() {
         Pagereference pageRef = new Pagereference ('/apex/HIPIVirtualFileRoom');
         
           /* Set<String> contractId = new Set<String>();
            Set<String> docId = new Set<String>(); 
            
            for(ContentVersion contrId :[select Contract__c from ContentVersion] ) {        
                contractId.add(contrId.Contract__c);                  
            } 
            system.debug('ContentDocumentId' + contractId);
            
            for(ContentVersion conDocId :[select contentDocumentId from ContentVersion] ) {        
                docId.add(conDocId.contentDocumentId);                  
            }
            system.debug('ContentDocumentId'+docId);*/
 /*           return pageRef;
     }  
             
}
*/