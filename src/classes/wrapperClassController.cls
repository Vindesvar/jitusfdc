public with sharing class wrapperClassController
 {

    public List<wrapAccount> wrapAccountList{get;set;}
    public List<Account> SelectedAccounts{get;set;}
    private integer counter=0;  //keeps track of the offset
    private integer list_size=10; //sets the page size or number of rows
    public integer total_size; //used to show user the total size of the list
   
    Set<ID> accId = new Set<Id>();
    
    public wrapperClassController() {
        wrapAccountList = new List<wrapAccount>();
        total_size = [select count() from Account];
        selectedPage='0';
        
    }
    

    
    
    //Pagination

    
    public List<wrapAccount> getNumbers() {
        if (selectedPage != '0') 
           counter = list_size*integer.valueOf(selectedPage)-list_size;    
            wrapAccountList.clear();
    try{
            for(Account a : [Select Id, Name, Type, Industry, Phone, Fax from Account limit : list_size offset :counter]) {
            
               wrapAccountList.add(new wrapAccount(a)); 
               accId.add(a.id);
            }
            return wrapAccountList;
        }
        catch (QueryException e) {                            
                ApexPages.addMessages(e);
                return null;
        } 
    }
    

        
    public string selectedPage{get;set{selectedPage=value;}
    }
    
    /*public dynamic_visualforce_with_soql_offset() {
        total_size = [select count() from Number__c]; //set the total size in the constructor
        selectedPage='0';
    }*/
    
  /*  public Account[] getNumbers() {
                
        if (selectedPage != '0') 
           counter = list_size*integer.valueOf(selectedPage)-list_size;
        try { //we have to catch query exceptions in case the list is greater than 2000 rows
                Account[] numbers = [select Id, Name, Type, Industry, Phone, Fax
                                     from Account 
                                     order by Account
                                     limit :list_size
                                    offset :counter];                   
                return numbers;
        
        } catch (QueryException e) {                            
                ApexPages.addMessages(e);                   
                return null;
        }        
    } */
    
    public Component.Apex.pageBlockButtons getMyCommandButtons() {
        
        //the reRender attribute is a set NOT a string
        Set<string> theSet = new Set<string>();
        theSet.add('myPanel');
        theSet.add('myButtons');
                
        integer totalPages;
        if (math.mod(total_size, list_size) > 0) {
            totalPages = total_size/list_size + 1;
        } else {
            totalPages = (total_size/list_size);
        }
        
        integer currentPage;        
        if (selectedPage == '0') {
            currentPage = counter/list_size + 1;
        } else {
            currentPage = integer.valueOf(selectedPage);
        }
     
        Component.Apex.pageBlockButtons pbButtons = new Component.Apex.pageBlockButtons();        
        pbButtons.location = 'top';
        pbButtons.id = 'myPBButtons';
        
        Component.Apex.outputPanel opPanel = new Component.Apex.outputPanel();
        opPanel.id = 'myButtons';
                                
        //the Previous button will alway be displayed
        Component.Apex.commandButton b1 = new Component.Apex.commandButton();
        b1.expressions.action = '{!Previous}';
        b1.title = 'Previous';
        b1.value = 'Previous';
        b1.expressions.disabled = '{!disablePrevious}';        
        b1.reRender = theSet;

        opPanel.childComponents.add(b1);        
                        
        for (integer i=0;i<totalPages;i++) {
            Component.Apex.commandButton btn = new Component.Apex.commandButton();
            
            if (i+1==1) {
                btn.title = 'First Page';
                btn.value = 'First Page';
                btn.rendered = true;                                        
            } else if (i+1==totalPages) {
                btn.title = 'Last Page';
                btn.value = 'Last Page';
                btn.rendered = true;                            
            } else {
                btn.title = 'Page ' + string.valueOf(i+1) + ' ';
                btn.value = ' ' + string.valueOf(i+1) + ' ';
                btn.rendered = false;             
            }
            
            if (   (i+1 <= 5 && currentPage < 5)
                || (i+1 >= totalPages-4 && currentPage > totalPages-4)
                || (i+1 >= currentPage-2 && i+1 <= currentPage+2))
            {
                btn.rendered = true;
            }
                                     
            if (i+1==currentPage) {
                btn.disabled = true; 
                btn.style = 'color:blue;';
            }  
            
            btn.onclick = 'queryByPage(\''+string.valueOf(i+1)+'\');return false;';
                 
            opPanel.childComponents.add(btn);
            
            if (i+1 == 1 || i+1 == totalPages-1) { //put text after page 1 and before last page
                Component.Apex.outputText text = new Component.Apex.outputText();
                text.value = '...';        
                opPanel.childComponents.add(text);
            } 
             
        }
        
        //the Next button will alway be displayed
        Component.Apex.commandButton b2 = new Component.Apex.commandButton();
        b2.expressions.action = '{!Next}';
        b2.title = 'Next';
        b2.value = 'Next';
        b2.expressions.disabled = '{!disableNext}';        
        b2.reRender = theSet;
        opPanel.childComponents.add(b2);
                
        //add all buttons as children of the outputPanel                
        pbButtons.childComponents.add(opPanel);  
  
        return pbButtons;

    }    
    
    public PageReference refreshGrid() { //user clicked a page number        
        system.debug('**** ' + selectedPage);
        return null;
    }
    
    public PageReference Previous() { //user clicked previous button
        selectedPage = '0';
        counter -= list_size;
        return null;
    }

    public PageReference Next() { //user clicked next button
        selectedPage = '0';
        counter += list_size;
        return null;
    }

    public PageReference End() { //user clicked end
        selectedPage = '0';
        counter = total_size - math.mod(total_size, list_size);
        return null;
    }
    
    public Boolean getDisablePrevious() { //this will disable the previous and beginning buttons
        if (counter>0) return false; else return true;
    }

    public Boolean getDisableNext() { //this will disable the next and end buttons
        if (counter + list_size < total_size) return false; else return true;
    }

    public Integer getTotal_size() {
        return total_size;
    }
    
    public Integer getPageNumber() {
        return counter/list_size + 1;
    }

    public Integer getTotalPages() {
        if (math.mod(total_size, list_size) > 0) {
            return total_size/list_size + 1;
        } else {
            return (total_size/list_size);
        }
    }
    public void testSOQLOffsetExampleController() {
        wrapperClassController c = new wrapperClassController();        
        c.getNumbers();    
        c.getDisableNext();
        c.getDisablePrevious();
        c.getTotal_size();
        c.Next();
        c.Next();
        c.Previous();  
        c.End();    
        c.getMyCommandButtons();
        c.SelectedPage='5';
        c.total_size=27;
        c.refreshGrid();        
        c.getMyCommandButtons(); 
        c.getPageNumber();
        c.getTotalPages();                       
    }    
    //Pagination
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
    public PageReference processSelected() {
        selectedAccounts = new List<Account>();
       
        for(wrapAccount wrapObj : wrapAccountList){
            if(wrapObj.isSelected == true ) {
            SelectedAccounts.add(wrapObj.accn);
             }
        }
        if(SelectedAccounts.size()>0){}else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please select Checkbox to update'));}
       Return null; 
    }
    
    List<Account> toUpdate=new List<Account>();
    public PageReference updateToAccName(){
    
        selectedAccounts = new List<Account>();
       
        for(wrapAccount wrapObj : wrapAccountList){
            if(wrapObj.isSelected == true ) {
            SelectedAccounts.add(wrapObj.accn);
             }
        }    
    
        for(Account ac : SelectedAccounts) {
        ac.Name = 'Jitu Kumar Gupta';
        toUpdate.add(ac);
        }
        update toUpdate;         
        return null;
    }

    public class wrapAccount {
        public Account accn {get; set;}
        public Boolean isSelected { get; set; }
        
        public wrapAccount (Account a) {
            accn = a;
            isSelected = false;
        }
    }
}