trigger demo291Trigger on Account (before insert, after insert) {
    
    if(trigger.isInsert && trigger.isBefore) {
        demo291accountTrgHandler.validateAccount(trigger.new);
    }
    
    if(trigger.isInsert && trigger.isAfter) {
        demo291accountTrgHandler.createContact(trigger.new);
    }
    
}