trigger UpdateAccountPhone on Contact (before update,after update) {
    
    if(checkRecursiveTriggerClass.firstRun){
        checkRecursiveTriggerClass.firstRun = false;
        List<String> ListOfPhone = new List<String>();
        List<Account> listToUpdaetPhone = new List<Account>();
        List<ID> listOfAccountId = new List<ID>();
        if(trigger.isUpdate){
            for (Contact updatedContact : [Select Id, Phone, AccountId from contact where Id in : trigger.new ]){    
                ListOfPhone.add(updatedContact.Phone);
                listOfAccountId.add(updatedContact.AccountId);
            }
        }
        Integer i = 0;
        For(Account acc : [select id, Phone from Account where ID in : listOfAccountId ]){
            acc.Phone = ListOfPhone[i];
            listToUpdaetPhone.add(acc);
        }
        if(listToUpdaetPhone.size()>0){
             update listToUpdaetPhone;         
        }        
    }
      
}