/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CloudAmpGABatchFetch implements Database.AllowsCallouts, Database.Batchable<User>, Database.Stateful {
    global List<String> debugMsgs;
    global String instance;
    global CloudAmpGA.CloudAmpGAApiManager manager;
    global List<String> messages;
    global List<CloudAmpGA__CloudAmpGA_Metrics__c> metricsData;
    global CloudAmpGA.CloudAmpGAProfiles profiles;
    global CloudAmpGABatchFetch() {

    }
    global void execute(Database.BatchableContext bc, List<User> users) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global System.Iterable start(Database.BatchableContext bc) {
        return null;
    }
}
